<?php

namespace Drupal\ckeditor_language\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "language" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_language",
 *   label = @Translation("CKEditor Language Button"),
 *   module = "ckeditor_language"
 * )
 */
class LanguageButton extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $path = 'libraries/language/plugin.js';
    if (\Drupal::moduleHandler()->moduleExists('libraries')) {
      $path = libraries_get_path('language') . '/plugin.js';
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
