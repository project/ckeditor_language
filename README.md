# CKEditor Language Button

INTRODUCTION
------------

This module provides integration with the Language plugin for CKEditor.

    This plugin implements the Language toolbar button to support the 
    WCAG 3.1.2 Language of Parts specification.


REQUIREMENTS
------------

 * Core CKEditor module
 * The CKEditor Language plugin


INSTALLATION
------------

    1. Download the plugin from https://ckeditor.com/cke4/addon/language
    2. Place the plugin in the libraries folder (/libraries/language)
    3. Enable CKEditor Language Button module
    4. Drag the new Language button into your toolbar for the format of choice
    5. Change the settings for the plugin if desired
    6. Create some content, and within the editor, select the text which you
       want to specify the language for
    7. Click the Language button and select the desired language


CONFIGURATION
-------------

 * Edit the Text format you wish to add the Language button for at
   Administration » Configuration » Text formats and editors
 * Drag the Language button into the toolbar
 * Under "CKEditor plugin settings", select the Language vertical-tab
 * Make a choice between "United Nations official languages" and "All 95
   languages". The first option will only show the six official languages of
   the UN. The extended list will show all 95 languages that are available in
   Drupal.
